# Ejercicio 2: Escribe otro programa que pida una lista de números como la anterior y al ﬁnal muestre por
# pantalla el máximo y mínimo de los números, en vez de la media.

# autora ="Karen Paladines"
# email  ="karen.paladines@unl.edu.ec"

cont =0
list= []
total=0

while True:

    valor = input("Intoduce un numero entero (o 'fin' para terminar):")
    list.append(valor)
    if valor.lower() in "fin":
        break
    try:
        total += float(valor)
        cont += 1
        max1 = max(list)
        min1 = min(list)
    except ValueError:
        print("Valor introducido inccorrecto. Ingrese el valor nuevamente..")

print("El total es->:",total)
print("Haz introducido->:",cont,"numeros")
print("Lista de numeros->", list)
print("El numero mayor ingresado es->:",float(max1))
print("El numero menor ingresado es->:",float(min1))

