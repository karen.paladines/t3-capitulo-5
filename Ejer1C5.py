# Ejercicio 1: Escribe un programa que lea repetidamente números hasta que el usuario
# introduzca “ﬁn”. Una vez se haya introducido “ﬁn”, muestra por pantalla el total,
# la cantidad de números y la media de esos números. Si el usuario introduce cualquier
# otra cosa que no sea un número, detecta su fallo usando try y except, muestra un mensaje
# de error y pasa al número siguiente.

# autora ="Karen Paladines"
# email  ="karen.paladines@unl.edu.ec"

cont=0
total=0

while True:
    valor =input("Introduzca un numero enter (o'fin' para terminar):")
    if valor.lower() in "fin":
        break
    try:
        total += int(valor)
        cont +=1
        media =total/cont
    except ValueError:
        print("Valor introducido incorrecto. Ingrese el valor nuevamente..")

print("El total es ->:", total)
print("Haz introducido->:", cont, "numeros")
print("La media de los valores es->:",total / cont)

